<?php
	function call($controller, $action) {
		require_once('controllers/' .$controller . '_controller.php');

		switch ($controller) {
			case 'pages':
				$controller = new PagesController();
				break;
			case 'usermanager':
				//require_once('models/user_manager.php');
				$controller = new UserManagerController();
				break;
		}

		$controller->{$action}();
	}

	$controllers = array('pages' => ['home', 'newsfeed', 'profile', 'reg', 'reg_success', 'login_success', 'error'],
						'usermanager' => ['login_na', 'logout_na', 'register_na']);

	if(array_key_exists($controller, $controllers)) {
		if(in_array($action, $controllers[$controller])) {
			call($controller, $action);
		} else {
			call('pages', 'error');
		}
	} else {
		call('pages', 'error');
	}
?>