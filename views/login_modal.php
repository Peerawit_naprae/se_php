<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="js/sha512.js"></script>
	<script type="text/javascript" src="js/form.js"></script>
</head>
<body>
<div id="login_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="post" action="index.php?controller=usermanager&action=login_na" id="login_form">
				<div class="modal-header">
					<h3>Login</h3>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="email" placeholder="Email" name="email">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" placeholder="Password" name="password">
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default" onclick="return formhash(document.getElementById('login_form'));">Login</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>