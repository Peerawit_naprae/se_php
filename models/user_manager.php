<?php
	class UserManager
	{
		public $db;
		function __construct()
		{
			$this->db = Db::getInstance();
		}
		public function sec_session_start() {
			$secure = true;

			$httponly = true;
			if(ini_set('session.use_only_cookies', 1) === FALSE) {
				echo ini_set('session.use_only_cookies', 1);
				exit();
			}

			$cookieParams = session_get_cookie_params();
			session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
			session_name("uniapp");
			session_start();
			session_regenerate_id();
		}
		public function login($email, $password) {
			if($stmt = $this->db->prepare("SELECT id, username, password, user_status FROM users WHERE email = ? LIMIT 1")) {
				$stmt->bind_param('s', $email);
				$stmt->execute();
				$stmt->store_result();

				$stmt->bind_result($user_id, $username, $db_password, $user_status);
				$stmt->fetch();

				if($stmt->num_rows == 1) {
					if(password_verify($password, $db_password)) {
						$user_browser = $_SERVER['HTTP_USER_AGENT'];
						$user_id = preg_replace("/[^0-9]+/", "", $user_id);
						$_SESSION['user_id'] = $user_id;
						$username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
						$_SESSION['username'] = $username;
						$_SESSION['login_string'] = hash('sha512', $db_password . $user_browser);
						$_SESSION['user_status'] = $user_status;
						// login successful
						return true;
					} else {
						//password is incorrect
						return false;
					}
				} else {
					// No user exists;
					return false;
				}
			}
		}
		public function login_check() {
			if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'], $_SESSION['user_status'])){
				$user_id = $_SESSION['user_id'];
				$login_string = $_SESSION['login_string'];
				$username = $_SESSION['username'];
				$user_status = $_SESSION['user_status'];

				$user_browser = $_SERVER['HTTP_USER_AGENT'];

				if($stmt = $this->db->prepare("SELECT password FROM users WHERE id = ? LIMIT 1")) {
					$stmt->bind_param('i', $user_id);
					$stmt->execute();
					$stmt->store_result();

					if($stmt->num_rows == 1) {
						$stmt->bind_result($password);
						$stmt->fetch();
						$login_check = hash('sha512', $password . $user_browser);
						if(hash_equals($login_check, $login_string)) {
							// Logged In
							return true;
						} else {
							// Not logged in
							return false;
						}
					} else {
						// Not logged in
						return false;
					}
				} else {
					// Not logged in
					return false;
				}
			} else {
				// Not logged in
				return false;
			}
		}
		//******
		public function create_profile($username){
			$stmt = $this->db->prepare("INSERT INTO profiles (username) VALUES (?)");
			if($stmt){
				$stmt->bind_param('s', $username);
				$stmt->execute();
			} else {
				header('Location: ../views/pages/error.php?err=Registeration failure: Profiles');
			}
		}
		public function register() {
			if(isset($_POST['username'], $_POST['email'], $_POST['p'])) {
				$username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
				$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
				if(!filter_var($email, FILTER_SANITIZE_STRING)) {
					// Not a valid email
					echo 'Invalid Email';
					exit();
				}
				$password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
				if(strlen($password) != 128) {
					echo 'Invalid Password Config';
					exit();
				}
				$prep_stmt = "SELECT id FROM users WHERE email = ? LIMIT 1";
				$stmt = $this->db->prepare($prep_stmt);

				if($stmt) {
					$stmt->bind_param('s', $email);
					$stmt->execute();
					$stmt->store_result();

					if($stmt->num_rows == 1) {
						echo 'Email already exists';
						exit();
						$stmt->close();
					}
				} else {
					echo 'Database Error';
					exit();
					$stmt->close();
				}
				$prep_stmt = "SELECT id FROM users WHERE username = ? LIMIT 1";
				$stmt = $this->db->prepare($prep_stmt);

				if($stmt) {
					$stmt->bind_param('s', $username);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows == 1) {
						echo 'username already exists';
						exit();
					}
				} else {
					echo 'Database error';
					$stmt->close();
				}
				$password = password_hash($password, PASSWORD_BCRYPT);
				if($insert_stmt = $this->db->prepare("INSERT INTO users (username, email, password, user_status) VALUES (?,?,?,?)")) {
					$insert_stmt->bind_param('sssi', $username, $email, $password, $user_status);
					$user_status = 1;
					$this->create_profile($username);
					if(!$insert_stmt->execute()){
						header('Location: ../views/pages/error.php?err=Registeration failure: INSERT');
					}
				}
				header('Location: index.php?controller=pages&action=reg_success');
			}
		}
		public function admin_check($user_status){
			if($user_status == 0) {
				return true;
			}
			return false;
		}
	}
?>