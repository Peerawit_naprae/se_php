<?php
	class Db {
		private static $instance = NULL;

		public static function getInstance() {
			if(!isset(self::$instance)) {
				/*
				$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
				self::$instance = new PDO('mysql:host=localhost;dbname=uniapp','root', '', $pdo_options);*/
				self::$instance = new mysqli("localhost", "root", "", "uniapp");
				if(mysqli_connect_error()) {
					echo "MYSQL Error" . mysql_connect_error();
					exit();
				}
			}
			return self::$instance;
		}
	}
?>